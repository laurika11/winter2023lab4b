public class VirtualPetApp {
	
	public static void main(String[] args) {
		java.util.Scanner reader = new java.util.Scanner(System.in);
		int ARRAY_SIZE = 2;
		
		//Intialize array of bird instances from user input
		Bird[] flockOfBirds = new Bird[ARRAY_SIZE];
		for (int i = 0; i < flockOfBirds.length; i++) {
			
			System.out.println("Enter a bird type (Eagle, Owl, Crow, Flamingo):");
			String type = reader.nextLine();
			System.out.println("Enter the size of the bird (Small, Medium, Large):");
			String size = reader.nextLine();
			System.out.println("Can this type of bird fly? (true/false)");
			boolean canFly = Boolean.parseBoolean(reader.nextLine());
			flockOfBirds[i] = new Bird(type, size, canFly);
		}
		
		System.out.println(flockOfBirds[flockOfBirds.length-1].getType());
		System.out.println(flockOfBirds[flockOfBirds.length-1].getSize());
		System.out.println(flockOfBirds[flockOfBirds.length-1].getCanFly());
		
		System.out.println("Enter a bird type (Eagle, Owl, Crow, Flamingo) for the last bird again:");
		flockOfBirds[flockOfBirds.length-1].setType(reader.nextLine());
		System.out.println(flockOfBirds[flockOfBirds.length-1].getType());
		System.out.println(flockOfBirds[flockOfBirds.length-1].getSize());
		System.out.println(flockOfBirds[flockOfBirds.length-1].getCanFly());
	}
	
	
}
public class Bird {
	// Fields
	private String type;
	private String size;
	private boolean canFly;
	
	
	// Allows to bird to fly, unless the bird is unable..
	public void fly() {
		if (this.canFly) {
			System.out.println("Flap flap flap sooooooooar, flap flap soar..");
		} else {
			System.out.println("The " + this.type + " flaps it's wings, but nothings happens..");
		}
	}
	
	// Allows the bird to sing depending on the type
	public void sing() {
		if (this.type.equals("Eagle")) {
			System.out.println("Scraaaaaaw!!!");
		} else if (this.type.equals("Owl")) {
			System.out.println("Hooot hooot!");
		} else if (this.type.equals("Crow")) {
			System.out.println("Caaaaw caaaw!");
		} else {
			System.out.println("Honk grunt growl!");
		}
		
	}
	
	public void setType(String type){
		this.type = type;
	}
	
	public String getType(){
		return this.type;
	}
	
	public String getSize(){
		return this.size;
	}
	
	public boolean getCanFly(){
		return this.canFly;
	}
	
	public Bird(String type, String size, boolean canFly){
		this.type = type;
		this.size = size;
		this.canFly = canFly;
	}
	
}